import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GridRoutingModule } from './grid-routing.module';
import { ListarTodosComponent } from './listar-todos/listar-todos.component';
import { AgGridModule } from 'ag-grid-angular';


@NgModule({
  declarations: [
    ListarTodosComponent
  ],
  imports: [
    CommonModule,
    GridRoutingModule,
    AgGridModule.withComponents([])
  ],
  exports: [
    ListarTodosComponent
  ]
})
export class GridModule { }
