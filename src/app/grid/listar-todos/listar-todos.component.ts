import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listar-todos',
  templateUrl: './listar-todos.component.html',
  styleUrls: ['./listar-todos.component.css'],
})
export class ListarTodosComponent implements OnInit {
  private gridApi: any;
  private gridColumnApi: any;

  private defaultColDef: any;

  savedFilterModel: any;

  columnDefs = [
    { field: 'make', sortable: true, filter: true },
    { field: 'model', sortable: true, filter: true },
    { field: 'price', sortable: true, filter: true },
  ];

  rowData = [];
  filterInstance: any;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http
      .get('https://www.ag-grid.com/example-assets/small-row-data.json')
      .subscribe((data: any) => {
        this.rowData = data;
      });
  }

  filter(params: any) {
    // Pega tudo que contém nos filtros
    this.savedFilterModel = this.gridApi.getFilterModel();

    //Retorna um array com os campos que possuem algum filtro
    var keys = Object.keys(this.savedFilterModel);

    for (const key in keys) {
      if (Object.prototype.hasOwnProperty.call(keys, key)) {
        const element = keys[key];
        // Campo
        console.log('Campo: ' + element);
        // valor digitado no campo
        console.log(
          'Valor digitado: ' + this.savedFilterModel[element]?.filter
        );
      }
    }
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
}
